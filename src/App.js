import logo from './logo.svg';
import './App.css';
import Reporter from './Reporter.js';

function App() {
  return (
    <div className="App">
      <img src="https://t3.gstatic.com/images?q=tbn:ANd9GcST0uhyr3xB7pcZ2VXfKvwUzo1gTRw47bHJE3uIu530516mW1XsZgZG1HVaXKTm6rJU88yuKQ"></img>
      <Reporter name="Antero Mertaranta">Löikö morkö sisään?</Reporter>
      <Reporter name="Kevin McGran">I know it's a rough time now, but did you at least enjoy playing in the tournament</Reporter>
    </div>
  );
}

export default App;
